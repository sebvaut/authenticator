package authenticator

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

// UserInfo contains the info from the auth token
type UserInfo struct {
	Aud            []string `json:"aud"`
	AuthTime       int      `json:"auth_time"`
	ResourceAccess map[string]struct {
		Roles []string `json:"roles"`
	} `json:"resource_access"`
	Roles             []string `json:"roles"`
	Name              string   `json:"name"`
	PreferredUsername string   `json:"preferred_username"`
	GivenName         string   `json:"given_name"`
	FamilyName        string   `json:"family_name"`
	Email             string   `json:"email"`
}

const authCookieStoreName = "authstore"
const defaultFallbackSeed = "impEdfJyeIpDtUOSwMysMIaQYdeONTZ"

var errNoAuthHeaderOrCookie = errors.New("no Authorization header or auth cookie")
var errInvalidTokenFormat = errors.New("invalid token format")

// Config contains the params needed for the Authenticator
type Config struct {
	IssuerURL              string
	AuthRedirectURL        string
	LoginLogoutRedirectURL string
	ClientID               string
	ClientSecret           string
}

// Authenticator contains all the data required to handle the auth process
type Authenticator struct {
	ctx                    context.Context
	oauth2Config           oauth2.Config
	verifier               *oidc.IDTokenVerifier
	LoginLogoutRedirectURL string
	IssuerURL              string
	cookieStore            *sessions.CookieStore
}

// NewAuthenticator returns a new Authenticator instance
func NewAuthenticator(conf Config, store *sessions.CookieStore) (*Authenticator, error) {
	return NewAuthenticatorWithContext(context.Background(), conf, store)
}

// NewAuthenticatorWithContext returns a new Authenticator instance with the provided context
func NewAuthenticatorWithContext(ctx context.Context, conf Config, store *sessions.CookieStore) (*Authenticator, error) {
	auth := &Authenticator{}
	auth.ctx = ctx
	provider, err := oidc.NewProvider(ctx, conf.IssuerURL)
	if err != nil {
		return nil, err
	}
	auth.oauth2Config = oauth2.Config{
		ClientID:     conf.ClientID,
		ClientSecret: conf.ClientSecret,
		RedirectURL:  conf.AuthRedirectURL,
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}

	auth.LoginLogoutRedirectURL = conf.LoginLogoutRedirectURL
	auth.IssuerURL = conf.IssuerURL

	auth.verifier = provider.Verifier(&oidc.Config{
		ClientID: conf.ClientID,
	})

	auth.cookieStore = store

	return auth, nil
}

// CallBackHandler is the handler for the auth callback URL
func (auth *Authenticator) CallBackHandler(w http.ResponseWriter, r *http.Request) {
	_, stateOK := auth.checkState(r)

	if !stateOK {
		http.Error(w, "state did not match", http.StatusBadRequest)
		return
	}

	oauth2Token, err := auth.oauth2Config.Exchange(auth.ctx, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
		return
	}
	_, err = auth.verifier.Verify(auth.ctx, rawIDToken)
	if err != nil {
		http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	if auth.cookieStore != nil {
		s, err := auth.cookieStore.Get(r, authCookieStoreName)
		if err == nil {
			s.Values["auth"] = "Bearer " + oauth2Token.AccessToken
			s.Save(r, w)
			http.Redirect(w, r, auth.LoginLogoutRedirectURL, http.StatusTemporaryRedirect)
		}
	}
	fmt.Fprint(w, oauth2Token.AccessToken)

}

// Check returns an error if the given request is unauthenticated or if there was an issue verifying the request
func (auth *Authenticator) Check(r *http.Request) (rawToken []byte, status int, err error) {
	var rawAccessToken string
	if auth.cookieStore != nil {
		s, err := auth.cookieStore.Get(r, authCookieStoreName)
		if err == nil {
			auth, ok := s.Values["auth"]
			if ok {
				rawAccessToken = auth.(string)
			}
		}
	}
	if rawAccessToken == "" {
		rawAccessToken = r.Header.Get("Authorization")
	}

	if rawAccessToken == "" {
		return nil, http.StatusUnauthorized, errNoAuthHeaderOrCookie
	}

	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		return nil, http.StatusBadRequest, errInvalidTokenFormat
	}

	tb, err := base64.RawStdEncoding.DecodeString(strings.Split(parts[1], ".")[1])
	if err != nil {
		return nil, http.StatusBadRequest, errInvalidTokenFormat
	}

	_, err = auth.verifier.Verify(auth.ctx, parts[1])
	if err != nil {
		return tb, http.StatusUnauthorized, err
	}

	return tb, http.StatusOK, nil
}

// HasRole returns true if the given user has the given role
func (auth *Authenticator) HasRole(role string, user *UserInfo) bool {
	for _, r := range user.Roles {
		if r == role {
			return true
		}
	}
	return false
}

// RequiresRoleMW returns a HTTP 403 error if the given role is not present
func (auth *Authenticator) RequiresRoleMW(role string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, err := auth.User(r, nil)
		if err != nil {
			http.Error(w, "An error occured", http.StatusInternalServerError)
			return
		}

		if !auth.HasRole(role, user) {
			http.Error(w, "Missing role", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// CheckWithRedirect redirects to the auth provider if the request is unauthenticated
func (auth *Authenticator) CheckWithRedirect(w http.ResponseWriter, r *http.Request) (err error) {
	_, _, err = auth.Check(r)
	if err != nil {
		if strings.Contains(err.Error(), "expected audience") {
			http.Error(w, "You are not authorized to use this app", http.StatusUnauthorized)
			return err
		}
		http.Redirect(w, r, auth.oauth2Config.AuthCodeURL(auth.newState(w, r)), http.StatusFound)
		return err
	}

	return nil
}

// CheckMW returns an http error if the request is unauthenticated
func (auth *Authenticator) CheckMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, status, err := auth.Check(r)
		if err != nil {
			http.Error(w, err.Error(), status)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// CheckRedirectMW redirects to the auth provider if the request is unauthenticated
func (auth *Authenticator) CheckRedirectMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := auth.CheckWithRedirect(w, r)
		if err != nil {
			return
		}
		next.ServeHTTP(w, r)
	})
}

// User returns the user info from the auth token
// Give it the http Request, or nil and the rawTokenString received by the Check func
func (auth *Authenticator) User(r *http.Request, rawToken []byte) (info *UserInfo, err error) {
	if r != nil {
		rawToken, _, err = auth.Check(r)
		if err != nil {
			return nil, err
		}
	}
	info = &UserInfo{}
	err = json.Unmarshal(rawToken, info)
	if err != nil {
		return nil, err
	}

	for c, v := range info.ResourceAccess {
		if c == auth.oauth2Config.ClientID {
			info.Roles = v.Roles
		}
	}

	return info, err
}

// LogoutHandler clears the cookie auth store and redirects the used to the oidc provider to logout
func (auth *Authenticator) LogoutHandler(w http.ResponseWriter, r *http.Request) {
	if auth.cookieStore != nil {
		s, err := auth.cookieStore.Get(r, authCookieStoreName)
		if err == nil {
			s.Values["auth"] = ""
			s.Save(r, w)
		}
	}

	http.Redirect(w, r, auth.IssuerURL+"/protocol/openid-connect/logout?redirect_uri="+url.QueryEscape(auth.LoginLogoutRedirectURL), http.StatusFound)
	return
}

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func (auth *Authenticator) newState(w http.ResponseWriter, r *http.Request) string {
	u := url.QueryEscape(r.URL.String())
	if auth.cookieStore != nil {
		s, err := auth.cookieStore.Get(r, authCookieStoreName)
		if err == nil {
			rand.Seed(time.Now().UnixNano())

			seed := randStringRunes(16)
			state := u + "---" + seed
			s.Values["seed"] = seed
			s.Save(r, w)
			return state
		}
	}

	state := u + "---" + defaultFallbackSeed
	return state
}

func (auth *Authenticator) checkState(r *http.Request) (url string, match bool) {
	urlState := r.URL.Query().Get("state")
	stateParts := strings.Split(urlState, "---")
	if len(stateParts) != 2 {
		return "", false
	}
	u := stateParts[0]
	s := stateParts[1]

	myStateSeed := defaultFallbackSeed
	if auth.cookieStore != nil {
		s, err := auth.cookieStore.Get(r, authCookieStoreName)
		if err == nil {
			stateR, ok := s.Values["seed"]
			if ok {
				myStateSeed = stateR.(string)
			}
		}
	}
	if myStateSeed != s {
		return u, false
	}

	return u, true
}
